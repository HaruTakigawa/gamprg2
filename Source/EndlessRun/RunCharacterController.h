// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "RunCharacterController.generated.h"

/**
 * 
 */
UCLASS()
class ENDLESSRUN_API ARunCharacterController : public APlayerController
{
	GENERATED_BODY()

private:
	class ARunCharacter* runChar;

public:
	ARunCharacterController();

protected:
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupInputComponent() override;

	void MoveForward(float Value);
	void MoveRight(float Value);
};
