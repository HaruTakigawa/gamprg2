// Fill out your copyright notice in the Description page of Project Settings.

#include "RunCharacterController.h"
#include "RunCharacter.h"

ARunCharacterController::ARunCharacterController() 
{

}
void ARunCharacterController::BeginPlay()
{
	Super::BeginPlay();
	runChar = Cast<ARunCharacter>(GetPawn());
}
// Called to bind functionality to input
void ARunCharacterController::SetupInputComponent()
{
	Super::SetupInputComponent();
	InputComponent->BindAxis("MoveRight", this, &ARunCharacterController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ARunCharacterController::MoveRight);
}
void ARunCharacterController::MoveForward(float Value)
{
	// Find out which way is "forward" and record that the player wants to move that way.
	FVector Direction = FRotationMatrix(GetControlRotation()).GetScaledAxis(EAxis::X);
	runChar->AddMovementInput(Direction, 1);
}
void ARunCharacterController::MoveRight(float Value)
{
	// Find out which way is "forward" and record that the player wants to move that way.
	FVector Direction = FRotationMatrix(GetControlRotation()).GetScaledAxis(EAxis::Y);
	runChar->AddMovementInput(Direction, Value);
}
// Called every frame
void ARunCharacterController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
