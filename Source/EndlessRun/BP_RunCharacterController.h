// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "RunCharacterController.h"
#include "BP_RunCharacterController.generated.h"

/**
 * 
 */
UCLASS()
class ENDLESSRUN_API ABP_RunCharacterController : public ARunCharacterController
{
	GENERATED_BODY()
	
};
